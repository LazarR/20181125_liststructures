package array;

public class Main {

    public static void main(String[] args) {

        MyArrayList myArrayList1 = new MyArrayList();
        myArrayList1.add(4);
        myArrayList1.add(7);
        myArrayList1.add(3);
        myArrayList1.add(56);
        myArrayList1.add(334);
        myArrayList1.add(3345);
        myArrayList1.add(345);
        myArrayList1.add(657);
        myArrayList1.add(754);
        myArrayList1.add(234);
        myArrayList1.add(778);
        myArrayList1.add(5);
        myArrayList1.showArray();
        myArrayList1.add(76);
        myArrayList1.add(87);
        myArrayList1.add(56);
        myArrayList1.add(334);
        myArrayList1.add(3345);
        myArrayList1.add(345);
        myArrayList1.add(657);
        myArrayList1.add(754);
        myArrayList1.add(234);
        myArrayList1.add(334);
        myArrayList1.add(3345);
        myArrayList1.add(345);
        myArrayList1.add(657);
        myArrayList1.add(754);
        myArrayList1.add(234);
        myArrayList1.add(778);
        myArrayList1.showArray();
        myArrayList1.add(5);
        myArrayList1.add(754);
        myArrayList1.add(754);
        myArrayList1.add(754);
        myArrayList1.add(234);
        myArrayList1.showArray();

        System.out.println(myArrayList1.getAt(15));
        Integer exampleValue = myArrayList1.getAt(45);
        System.out.println(exampleValue);

    }
}
