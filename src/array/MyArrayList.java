package array;

public class MyArrayList {

    // se introduce o valoare statica (fixa) pentru array - 10 elemente
    static int DEFAULT_SEGMENT_LENGHT = 10;


    // array de valori
    int[] values;
    // lungime array
    int lenght;

    MyArrayList(){
        this.values = new int[DEFAULT_SEGMENT_LENGHT];
        this.lenght = 0;
    }

    void increaseSize(){

        //1. Create new array
        int[] newValues = new int[values.length + DEFAULT_SEGMENT_LENGHT];

        //2. Coppy values
        for (int i=0; i < values.length; i++){
            newValues[i] = values[i];
        }

        //3. Atribuirea valorilor

        values = newValues;
    }

    void add(int value){

        if (lenght == values.length){ // testam daca suntem la capacitate maxima. Daca da ... marim.
            increaseSize();
        }
        // set values
        values[lenght] = value;

        lenght++;  // increase current lenght
    }

    // Metoda de afisare a MyArrayList

    void showArray(){
        System.out.println(lenght + " / " + values.length);
        for (int i = 0; i < lenght; i++ ){
            System.out.print(values[i] + " ");
        }
        System.out.println();
    }

    Integer getAt(int index){
        if (index >= 0 && index < lenght){
            return values[index];
        }
        return null;
    }




}
